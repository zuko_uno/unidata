package main

import (
	"fmt"
	"launchpad.net/mgo"
	"launchpad.net/mgo/bson"
	"runtime"
	"strings"
	"unidata/foursquare"
	"unidata/wikipedia"
)

var (
	articles *mgo.Collection
	session  *mgo.Session
)

type Article struct {
	Id       bson.ObjectId "_id"
	Parent   bson.ObjectId "parent,omitempty"
	FParent  string        "fparent"
	For      string        "for"
	Language string        "language"
	Sections []Section     "sections"
}

type Section struct {
	Id      bson.ObjectId "_id"
	Text    string        "text"
	Caption string        "caption"
}

type Region struct {
	Id         bson.ObjectId "_id"
	Name       string        "name"
	Links      []string      "links"
	Collection string
}

func (r Region) getLang() string {
	return strings.Split(strings.Replace(r.Links[0], "http://", "", -1), ".")[0]
}

func (r Region) getTitle() string {
	tmp := strings.Replace(r.Links[0], "http://en.wikipedia.org/wiki/", "", -1)
	return strings.Replace(tmp, " ", "%20", -1)
}

func main() {
	var umobject *foursquare.CompactUmObject
	var err error
	const threads = 64

	runtime.GOMAXPROCS(runtime.NumCPU())

	//init db
	session, err = mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}
	session.SetMode(mgo.Monotonic, true)

	tasks := make(chan foursquare.CompactUmObject, threads)
	done := make(chan bool)

	ntf := make(chan string)
	go printer(ntf)

	for i := 0; i < threads; i++ {
		go runner(tasks, done, ntf)
	}

	articles = session.DB("unimaps").C("umobjects.articles")
	compacts := session.DB("unimaps").C("compactVenues").Find(nil).Iter()
	for {
		if !compacts.Next(&umobject) {
			break
		}
		tasks <- *umobject
	}

	for i := 0; i < threads; i++ {
		stop := foursquare.CompactUmObject{
			Name: "STOP",
		}
		tasks <- stop
		<-done
	}

	ntf <- "done"

}

func runner(tasks chan foursquare.CompactUmObject, done chan bool, ntf chan string) {
	var message string

	notify := func(msg string) {
		message += msg + "\n"
	}

	for {
		articleText := ""

		ok := false

		object := <-tasks
		if object.Name == "STOP" {
			break
		}

		message = ""

		notify(object.Name)
		title := wikipedia.SearchArticle(object.Name)
		if title != "" {
			percent := compare(object.Name, title)
			if percent >= 60 {
				ok = true
			}
			//fmt.Println(object.Name+" === "+title+":", percent, " :: ", avg)
		}
		if ok {
			articleText = wikipedia.RegionArticleGet(title)
			notify(fmt.Sprint("Load founded article: ", title))
		}

		if len(articleText) > 0 {
			section := Section{
				Id:      bson.NewObjectId(),
				Caption: title,
				Text:    articleText,
			}

			var sections []Section
			var err error
			sections = append(sections, section)

			article := Article{
				Id:       bson.NewObjectId(),
				FParent:  object.Id,
				For:      "foursquare",
				Language: "en",
				Sections: sections,
			}

			err = articles.Insert(article)
			if err != nil {
				notify(fmt.Sprint("Fail: error insert article: ", object.Name, err))
			} else {
				notify(fmt.Sprint("Article length: ", len(section.Text)))
				notify("Article created.")
			}
		} else {
			notify(fmt.Sprint("Article length: ", len(articleText)))
			notify("Article will be reloaded.")
		}

		ntf <- message
	}
	done <- true
}

func printer(input chan string) {
	i := 0
	for {
		msg := <-input
		i += 1
		if !strings.Contains(msg, "reloaded") {
			fmt.Println(i, " :: ", msg)
			if msg == "done" {
				return
			}
		}
	}
}

func compare(s1, s2 string) float32 {
	var (
		set1, set2           []string
		averageSize, percent float32
	)

	s1 = strings.ToLower(s1)
	s2 = strings.ToLower(s2)

	set1 = genSet(s1)
	set2 = genSet(s2)
	common := intersection(set1, set2)

	if float32(len(set1)) < float32(len(set2)) {
		averageSize = float32(len(set1))
	} else {
		averageSize = float32(len(set2))
	}
	percent = float32(len(common)) / averageSize * 100
	return percent
}

func genSet(s string) (set []string) {
	arr := strings.Split(s, "")
	for i := 0; i+2 < len(arr); i++ {
		set = append(set, strings.Join(arr[i:i+3], ""))
	}
	return
}

func intersection(set1, set2 []string) (result []string) {
	for _, s := range set1 {
		if r, pos := contains(set2, s); r {
			set2[pos] = set2[len(set2)-1]
			set2 = set2[0 : len(set2)-1]
			result = append(result, s)
		}
	}
	return
}

func contains(arr []string, str string) (bool, int) {
	for i, s := range arr {
		if s == str {
			return true, i
		}
	}
	return false, 0
}
