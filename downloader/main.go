package main

import (
	"fmt"
	"launchpad.net/mgo"
	"net/http"
	"time"
	"unidata/foursquare"
)

var (
	begin = "000000000000000000000000"
	end   = "001000000000000000000000"
	//2 begin   = "001000000000000000000000"
	//2 end     = "010000000000000000000000"
	//3 begin   = "010000000000000000000000"
	//3 end     = "011000000000000000000000"
	//4 begin   = "011000000000000000000000"
	//4 end     = "100000000000000000000000"
	venues  []foursquare.CompactUmObject
	count   int
	timenow int64
	limit   = 5000
)

type Sector struct {
	Id         string "_id"
	Updated    int64  "updated"
	Count      int    "count"
	NeedUpdate bool   "need_update"
}

func main() {
	//http client
	client := &http.Client{}

	//init db
	session, err := mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}
	session.SetMode(mgo.Monotonic, true)

	//init providers and start
	foursquare.InitAndListen(session, client)

	start := time.Now().Unix()
	qtree := begin

	for {
		for {
			if limit < 5 {
				if (time.Now().Unix()-start)/60 > 62 {
					limit = 5000
					start = time.Now().Unix()
					break
				} else {
					time.Sleep(time.Minute * 1)
				}
			} else {
				break
			}
		}

		qtree = next(qtree)
		if qtree != "100000000000000000000000" {
			venues = foursquare.FetchVenuesQtree(qtree)
			count = len(venues)
			foursquare.CacheVenues(venues)
			limit -= 1
			timenow = time.Now().Unix()

			//add to qtree base
			sector := Sector{
				Id:         qtree,
				Updated:    timenow,
				Count:      count,
				NeedUpdate: false,
			}
			err := session.DB("unimaps").C("sectors").Insert(sector)
			if err != nil {
				fmt.Println(err)
			}

			//print to log
			fmt.Println("Q:", qtree+" ", count, " limit=", limit)

		} else {
			return
		}
	}

}

func next(t string) string {
	i := len(t) - 1

	tt := []byte(t)

	for {
		if tt[i] == byte('0') {
			break
		} else {
			i = i - 1
		}
	}

	tt[i] = byte('1')

	for {
		i += 1
		if i < len(tt) {
			tt[i] = byte('0')
		} else {
			break
		}
	}

	return string(tt)
}
