package main

import (
	"fmt"
	"launchpad.net/mgo"
	"launchpad.net/mgo/bson"
	"unidata/foursquare"
)

func main() {
	var compact foursquare.CompactUmObject

	session, err := mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}
	session.SetMode(mgo.Monotonic, true)

	iter := session.DB("unimaps").C("compactVenues").Find(bson.M{"loc": bson.M{"$gte": 180}}).Iter()
	for {
		if !iter.Next(&compact) {
			break
		}
		fmt.Println(compact.Loc, " ", compact.Name, " ", compact.Id)
		err = session.DB("unimaps").C("compactVenues").Remove(bson.M{"_id": compact.Id})
		if err != nil {
			fmt.Println("Fail: error deleting: ", compact.Name)
		} else {
			fmt.Println("Deleted.")
		}
	}

	iter = session.DB("unimaps").C("compactVenues").Find(bson.M{"loc": bson.M{"$lte": -180}}).Iter()
	for {
		if !iter.Next(&compact) {
			break
		}
		fmt.Println(compact.Loc, " ", compact.Name, " ", compact.Id)
		err = session.DB("unimaps").C("compactVenues").Remove(bson.M{"_id": compact.Id})
		if err != nil {
			fmt.Println("Fail: error deleting: ", compact.Name)
		} else {
			fmt.Println("Deleted.")
		}
	}

}
