package wikipedia

import (
	"fmt"
	"strconv"
	"strings"
)

//wikisearch structures
type SearchJson struct {
	Query SearchQuery "query"
}

type SearchQuery struct {
	Search []Entry "search"
}

type Entry struct {
	Title     string "title"
	Wordcount int    "wordcount"
}

//wikilocation structures
type WlJson struct {
	Articles []WlNode "articles"
}

type WlNode struct {
	Distance string "distance"
	Title    string "title"
	Url      string "url"
}

type Article struct {
	Language string "language"
	Source   string "source"
	Text     string "text"
	Title    string "title"
}

func (art WlNode) close_enough() bool {
	i, err := strconv.Atoi(strings.Trim(art.Distance, "m"))
	if err != nil {
		return false
	}
	fmt.Println("distance: ", art.Distance, " ", i)
	if i < 1000 {
		return true
	}
	return false
}

//wikipedia structures
type WikiJson struct {
	Parse ParseJson "parse"
}

type ParseJson struct {
	Title string "title"
	Text  string "text"
}
