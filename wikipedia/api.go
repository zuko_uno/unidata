package wikipedia

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const (
	wpBase     = "http://en.wikipedia.org/w/api.php?action=parse&mobileformat=html&prop=text&format=json&page="
	wlBase     = "http://api.wikilocation.org/articles?format=json&locale=en&type=landmark"
	searchBase = "http://en.wikipedia.org/w/api.php?action=query&list=search&srprop=wordcount&format=json&srsearch="
)

func SearchArticle(query string) (articleName string) {
	var (
		node SearchJson
	)

	//fetch & parse article
	api := searchBase + strings.Replace(query, " ", "%20", -1)
	// fmt.Println("query is: ", api)
	response, err := http.Get(api)
	if err != nil {
		i := 0
		for {
			var err_reconnect error
			i++
			time.Sleep(10 * time.Millisecond)
			response, err_reconnect = http.Get(api)
			if err_reconnect == nil {
				break
			}
			if err_reconnect != nil && i >= 3 {
				return
			}
		}
	}

	jsonData, err := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	if err != nil {
		return
	}

	err = json.Unmarshal(jsonData, &node)

	if err != nil {
		//fmt.Println(err, " ", string(jsonData))
		//bad json, fuck that article!
		return
	}

	// fmt.Println("Wikiparse result: ")
	// fmt.Println(string(jsonData))

	if len(node.Query.Search) > 0 {
		match := node.Query.Search[0]
		if match.Wordcount > 10 {
			articleName = match.Title
		} else {
			articleName = ""
		}
	} else {
		articleName = ""
	}
	return
}

func FetchArticle(lat, lon float64) (result Article) {
	near := wikilocationGet(lat, lon)
	fmt.Println("Close enough = ", near.close_enough())
	if near.close_enough() {
		fmt.Println("Close enough inside")
		result = wikipediaGet(near)
		return
	}
	return
}

func wikilocationGet(lat, lon float64) (target WlNode) {
	var (
		node WlJson
	)

	//prepare params
	latS := strconv.FormatFloat(lat, 'f', -1, 64)
	lonS := strconv.FormatFloat(lon, 'f', -1, 64)
	apiUrl := wlBase + "&lat=" + latS + "&lng=" + lonS
	fmt.Println("wlGet: fetching : ", apiUrl)
	//fetch & parse article
	response, err := http.Get(apiUrl)
	if err != nil {
		fmt.Println("wlGet: connection error")
		i := 0
		for {
			var err_reconnect error
			i++
			time.Sleep(10 * time.Millisecond)
			response, err_reconnect = http.Get(apiUrl)
			if err_reconnect == nil {
				break
			}
			if err_reconnect != nil && i >= 3 {
				return
			}
		}
	}

	jsonData, err := ioutil.ReadAll(response.Body)
	defer response.Body.Close()
	//fmt.Println(string(jsonData))

	err = json.Unmarshal(jsonData, &node)

	if err != nil {
		//bad json, fuck that article!
		return
	}

	fmt.Println(node)
	if len(node.Articles) > 0 {
		target = node.Articles[0]
	}
	return
}

func wikipediaGet(a WlNode) (result Article) {
	var (
		node WikiJson
	)

	//fetch & parse article
	api := wpBase + strings.Replace(a.Title, " ", "_", -1)
	fmt.Println("wiki api url : ", api)
	response, err := http.Get(api)
	if err != nil {
		i := 0
		for {
			var err_reconnect error
			i++
			time.Sleep(10 * time.Millisecond)
			response, err_reconnect = http.Get(api)
			if err_reconnect == nil {
				break
			}
			if err_reconnect != nil && i >= 3 {
				return
			}
		}
	}

	jsonData, err := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	if err != nil {
		return
	}

	err = json.Unmarshal(jsonData, &node)

	if err != nil {
		fmt.Println(err, " ", string(jsonData))
		//bad json, fuck that article!
		return
	}

	fmt.Println("Wikiparse result: ")
	fmt.Println(string(jsonData))

	result.Language = "en"
	result.Source = a.Url
	result.Text = node.Parse.Text
	result.Title = node.Parse.Title
	return
}

func RegionArticleGet(name string) (text string) {

	//fetch & parse article
	api := "http://en.wikipedia.org/w/index.php?action=render&title=" + strings.Replace(name, " ", "_", -1)

	// fmt.Println("wiki api url : ", api)
	response, err := http.Get(api)
	if err != nil {
		i := 0
		for {
			var err_reconnect error
			i++
			time.Sleep(5 * time.Second)
			response, err_reconnect = http.Get(api)
			if err_reconnect == nil {
				break
			}
			if err_reconnect != nil && i >= 5 {
				return
			}
		}
	}

	htmlData, err := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	if err != nil {
		return
	}

	return string(htmlData)
}
