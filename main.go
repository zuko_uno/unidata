package main

/*Штука, которая обеспечивает систему информацией из foursquare, instagram, wikipedia и черт знает кого еще.
  Cleaner, categloader, downloader, wikiloader - это компилируемые скрипты, писал и запускал их в облаке,
  чтобы выкачать ту или иную инфу.*/

import (
	"launchpad.net/mgo"
	"net/http"
	"runtime"
	"unidata/foursquare"
	"unidata/instagram"
)

var (
	session  *mgo.Session
	response string
	err      error
)

func main() {
	//Рантайм пока не умеет сам все процы использовать, принуждаем его к этому
	runtime.GOMAXPROCS(runtime.NumCPU())

	//инициализируем сессию с базой данных
	session, err = mgo.Dial("localhost")
	session.SetMode(mgo.Monotonic, true)

	//хттп клиент навороченный в Го, заточен под использование из нескольких потоков
	client := &http.Client{}

	//каждый из провайдеров информации (форсквер, инстаграмм) - оформлены отдельно, лежат папочками
	//здесь вешаются их обработчики для определенных урлов. Метод initAndListen вешает обработчик на веб-сервер
	//для Го отдельный вебсервер тоже не нужен (если не нужны заморочки вроде кеширования), у него есть крутой встроенный
	//напрямую на порт вешается
	foursquare.InitAndListen(session, client)
	instagram.InitAndListen(session, client)

	//вешаем собственно сервер на порт
	http.ListenAndServe(":8090", nil)
}
