package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"launchpad.net/mgo"
	"net/http"
)

const (
	api = "https://api.foursquare.com/v2/venues/categories?client_id=5KCOBGCM3PCVD43MMZICRU2PRFN3GDYF5GNOABF4FBL3BE2Z&client_secret=FI55SJBX2LLJAMGOS2ECODGU0GSLOEYB0KJZ4LTJZVA40QU4&v=20120902"
)

type ApiResponse struct {
	Meta     MetaType               "meta"
	Response CategoriesResponseType "response"
}

type MetaType struct {
	Code int "code"
}

type CategoriesResponseType struct {
	Categories []FoursquareCategory "categories"
}

type FoursquareCategory struct {
	Id         string               "_id"
	Name       string               "name"
	Icon       IconType             "icon"
	Categories []FoursquareCategory "categories"
}

type IconType struct {
	Prefix string "prefix"
	Suffix string "suffix"
}

type UmCategory struct {
	Id     string "_id"
	Name   string "name"
	Icon   string "icon"
	Parent string "parent"
}

func main() {
	var categories ApiResponse
	//http client
	client := &http.Client{}

	//init db
	session, err := mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}
	session.SetMode(mgo.Monotonic, true)

	response, err := client.Get(api)
	jsonData, err := ioutil.ReadAll(response.Body)
	response.Body.Close()
	if err != nil {
		fmt.Println("Fetch details: error on read response")
		return
	}

	err = json.Unmarshal(jsonData, &categories)
	fmt.Println(categories)

	if err != nil {
		fmt.Println("Fail: parsing json: ", err)
		return
	}

	for _, c := range categories.Response.Categories {
		category := UmCategory{
			Id:   c.Id,
			Icon: c.Icon.Prefix + "bg_32" + c.Icon.Suffix,
			Name: c.Name,
		}
		err := session.DB("unimaps").C("categories").Insert(category)
		if err != nil {
			fmt.Println(err)
		}
		if len(c.Categories) > 0 {
			for _, c1 := range c.Categories {
				fmt.Println("____", c1.Id, " ", c1.Name)
				category := UmCategory{
					Id:     c1.Id,
					Icon:   c1.Icon.Prefix + "bg_32" + c1.Icon.Suffix,
					Name:   c1.Name,
					Parent: c.Id,
				}
				err := session.DB("unimaps").C("categories").Insert(category)
				if err != nil {
					fmt.Println(err)
				}
				if len(c1.Categories) > 0 {
					for _, c2 := range c1.Categories {
						fmt.Println("________", c2.Id, " ", c2.Name)
						category := UmCategory{
							Id:     c2.Id,
							Icon:   c2.Icon.Prefix + "bg_32" + c2.Icon.Suffix,
							Name:   c2.Name,
							Parent: c1.Id,
						}
						err := session.DB("unimaps").C("categories").Insert(category)
						if err != nil {
							fmt.Println(err)
						}
					}
				}
			}
		}
	}
}
