package foursquare

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

const (
	venuesEndpoint  = "https://api.foursquare.com/v2/venues/search"
	detailsEndpoint = "https://api.foursquare.com/v2/venues/"
	limit           = "&limit=50"
	radius          = "&radius=100000"
	intent          = "&intent=browse"
	clientId        = "client_id=5KCOBGCM3PCVD43MMZICRU2PRFN3GDYF5GNOABF4FBL3BE2Z"
	clientSecret    = "&client_secret=FI55SJBX2LLJAMGOS2ECODGU0GSLOEYB0KJZ4LTJZVA40QU4&v=20120902"
	//2 clientId        = "client_id=YTXINY1TBVGAEB1J1ROERXJIJ0YBYCIJYSLGYH1NDIQID3PQ"
	//2 clientSecret    = "&client_secret=ZNWXEGG1X1SJ1XHPDTNLCAVLTCFGDHXMWI0N413ETNC2HJJQ&v=20120902"
	//3 clientId        = "client_id=PB1EMKN5Q1E2J5ZPKBT0AJFLR4EDGQGIC11VIUO4XGA42ASX"
	//3 clientSecret    = "&client_secret=HQAQKATFPKWTX5MHHAKXNXSKIHOPQBPNP3DKBOE0M5UX2FXJ&v=20120902"
	//4 clientId     = "client_id=FG54RDFZGH3TYTAJYCJQKNFW4HRHMURVNWGCEWRWSGBVHKFA"
	//4 clientSecret = "&client_secret=VTWZCC0HSP0VPPZTAOCXWCVIBTFYCW1ALSBIIQLOUAQ0WVXD&v=20120902"
)

func fetchDetails(id string) (complete CompleteVenue) {
	var details DetailsApiResponse

	apiUrl := detailsEndpoint + id + "?" + clientId + clientSecret

	//fetch and parse
	response, err := httpClient.Get(apiUrl)
	if err != nil {
		fmt.Println("Fetch details: error on connection")
		i := 0
		for {
			var err_reconnect error
			i++
			time.Sleep(10 * time.Millisecond)
			response, err_reconnect = http.Get(apiUrl)
			if err_reconnect == nil {
				break
			}
			if err_reconnect != nil && i >= 2 {
				return
			}
		}
	}

	jsonData, err := ioutil.ReadAll(response.Body)
	response.Body.Close()

	if err != nil {
		fmt.Println("Fetch details: error on read response")
		return
	}

	err = json.Unmarshal(jsonData, &details)

	if err != nil {
		fmt.Println("Fail: parsing json: ", err)
		return
	}

	complete = details.Response.Venue

	return
}

func fetchVenues(lat, lon float64) (venuesData []CompactUmObject) {
	var venues VenuesApiResponse

	apiUrl := venuesEndpoint + "?ll=" + prepareLL(lat, lon)
	apiUrl += limit + radius + intent + "&" + clientId + clientSecret

	fmt.Println("Fetch venues: inside")
	//fetch and parse
	response, err := httpClient.Get(apiUrl)
	if err != nil {
		fmt.Println("Fetch venues: http get error, reconnect")
		i := 0
		for {
			var err_reconnect error
			i++
			time.Sleep(10 * time.Millisecond)
			response, err_reconnect = httpClient.Get(apiUrl)
			if err_reconnect == nil {
				break
			}
			if err_reconnect != nil && i >= 2 {
				return
			}
		}
	}

	jsonData, err := ioutil.ReadAll(response.Body)
	response.Body.Close()

	if err != nil {
		fmt.Println("Fetch venues: error while reading response")
		return
	}

	fmt.Println("Fetch venues: response:")
	fmt.Println(string(jsonData))

	err = json.Unmarshal(jsonData, &venues)

	if err != nil {
		fmt.Println("Fetch venues: parsing json: ", err, jsonData)
		return
	}

	venuesData = apiVenues2um(venues.Response.Venues)

	fmt.Println("Fetch venues: call cache in routine")
	go cacheVenues(venuesData)

	fmt.Println("Fetch venues: sending response...")
	return
}

func power(num, pow int) (result int) {
	result = num
	for i := 1; i < pow; i = i + 1 {
		result = result * num
	}
	return
}

func qtree2rect(qtree string) (params string) {
	var (
		block_x, block_y int
		units            []string
	)

	for i := 0; i < len(qtree); i += 2 {
		units = append(units, qtree[i:i+2])
	}

	for i, duo := range units {
		switch duo {
		case "01":
			block_x += 4096 / power(2, i)
		case "10":
			block_y += 2048 / power(2, i)
		case "11":
			block_y += 2048 / power(2, i)
			block_x += 4096 / power(2, i)
		}
	}

	sw_lat := float64(block_y)*0.09000009 - 90.0 - 0.09000009
	sw_lon := float64(block_x)*0.09000009 - 180.0

	ne_lat := float64(block_y)*0.09000009 - 90.0
	ne_lon := float64(block_x)*0.09000009 - 180.0 + 0.09000009

	params = "?sw="
	params += strconv.FormatFloat(sw_lat, 'f', -1, 64) + ","
	params += strconv.FormatFloat(sw_lon, 'f', -1, 64) + "&ne="
	params += strconv.FormatFloat(ne_lat, 'f', -1, 64) + ","
	params += strconv.FormatFloat(ne_lon, 'f', -1, 64)

	return
}

func FetchVenuesQtree(qtree string) (venuesData []CompactUmObject) {
	var venues VenuesApiResponse

	apiUrl := venuesEndpoint + qtree2rect(qtree)
	apiUrl += limit + intent + "&" + clientId + clientSecret

	//fetch and parse
	response, err := httpClient.Get(apiUrl)
	if err != nil {
		fmt.Println("Fetch venues: http get error, reconnect")
		i := 0
		for {
			var err_reconnect error
			i++
			time.Sleep(10 * time.Millisecond)
			response, err_reconnect = httpClient.Get(apiUrl)
			if err_reconnect == nil {
				break
			}
			if err_reconnect != nil && i >= 2 {
				return
			}
		}
	}

	jsonData, err := ioutil.ReadAll(response.Body)
	response.Body.Close()

	if err != nil {
		fmt.Println("Fetch venues: error while reading response")
		return
	}

	err = json.Unmarshal(jsonData, &venues)

	if err != nil {
		fmt.Println("Fetch venues: parsing json: ", err, jsonData)
		return
	}

	venuesData = apiVenues2um(venues.Response.Venues)

	go cacheVenues(venuesData)

	return
}

func apiVenues2um(apiData []CompactVenue) (venueData []CompactUmObject) {
	for _, compact := range apiData {
		venueData = append(venueData, compact.toUmObject())
	}
	return
}

func prepareLL(lat, lon float64) (ll string) {
	fmt.Println("Preparing LL")
	ll += strconv.FormatFloat(lat, 'f', -1, 64)
	ll += ","
	ll += strconv.FormatFloat(lon, 'f', -1, 64)
	return
}
