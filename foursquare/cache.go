package foursquare

import (
	"fmt"
	"launchpad.net/mgo"
	"launchpad.net/mgo/bson"
)

func extractCachedVenues(box [2][2]float64, categories []string, territory string) (venues []CompactUmObject) {
	var compact CompactUmObject
	var query *mgo.Query

	if (categories[0] != "") && (territory != "") && (box[0][0] != 0) {
		query = s.DB("unimaps").C("compactVenues").
			Find(bson.M{"loc": bson.M{"$within": bson.M{"$box": box}}, "categories.id": bson.M{"$in": categories}, "geoparents": territory})
	}
	if (categories[0] != "") && (territory == "") && (box[0][0] != 0) {
		query = s.DB("unimaps").C("compactVenues").
			Find(bson.M{"loc": bson.M{"$within": bson.M{"$box": box}}, "categories.id": bson.M{"$in": categories}})
	}
	if (categories[0] == "") && (territory != "") && (box[0][0] != 0) {
		query = s.DB("unimaps").C("compactVenues").
			Find(bson.M{"loc": bson.M{"$within": bson.M{"$box": box}}, "geoparents": territory})
	}
	if (categories[0] == "") && (territory == "") && (box[0][0] != 0) {
		query = s.DB("unimaps").C("compactVenues").
			Find(bson.M{"loc": bson.M{"$within": bson.M{"$box": box}}})
	}

	//when don't have coords, select by category || territory
	if (categories[0] != "") && (territory != "") && (box[0][0] == 0) {
		query = s.DB("unimaps").C("compactVenues").
			Find(bson.M{"categories.id": bson.M{"$in": categories}, "geoparents": territory})
	}
	if (categories[0] != "") && (territory == "") && (box[0][0] == 0) {
		query = s.DB("unimaps").C("compactVenues").
			Find(bson.M{"categories.id": bson.M{"$in": categories}})
	}
	if (categories[0] == "") && (territory != "") && (box[0][0] == 0) {
		query = s.DB("unimaps").C("compactVenues").
			Find(bson.M{"geoparents": territory})
	}

	count, err := query.Count()
	if err != nil {
		fmt.Println("Error, while use count on query")
	}
	fmt.Println("Venues: found ", count)
	if count > 0 {
		iVenues := query.Iter()
		for {
			if !iVenues.Next(&compact) {
				break
			}
			venues = append(venues, compact)
		}
	}
	return
}

func extractCachedVenuesBackup(lat, lon, rad float64) (venues []CompactUmObject) {
	var compact CompactUmObject
	fmt.Println("Venues: extracting from cache...")
	ll := []float64{lat, lon}
	query := s.DB("unimaps").C("compactVenues").
		Find(bson.M{"loc": bson.M{"$near": ll, "$maxDistance": rad}})
	count, err := query.Count()
	if err != nil {
		fmt.Println("Error, while use count on query")
	}
	fmt.Println("Venues: found ", count)
	if count > 0 {
		iVenues := query.Iter()
		for {
			if !iVenues.Next(&compact) {
				break
			}
			venues = append(venues, compact)
		}
	}
	return
}

func cacheVenues(venuesData []CompactUmObject) {
	fmt.Println("Cache venues: saving venues")
	for _, venue := range venuesData {
		fmt.Println("Cache venues: add compact venue ", venue.Name)
		err := s.DB("unimaps").C("compactVenues").Insert(venue)
		if err != nil {
			fmt.Println(err)
		}
	}
	return
}

func CacheVenues(venuesData []CompactUmObject) {
	for _, venue := range venuesData {
		err := s.DB("unimaps").C("compactVenues").Insert(venue)
		if err != nil {
			fmt.Println(err)
		}
	}
	return
}

func cachePhotos(complete CompleteVenue) {
	for _, group := range complete.Photos.Groups {
		for _, photo := range group.Items {
			photo.Parent = complete.Id
			photo.UmSource = "foursquare"
			photo.Loc[0] = complete.Location.Lat
			photo.Loc[1] = complete.Location.Lng
			err := s.DB("unimaps").C("photos").Insert(photo)
			if err != nil {
				fmt.Println(err)
			}
		}
	}
	return
}

func extractCachedDetails(id string) (complete CompleteUmObject) {
	fmt.Println("Details: extracting from cache...")
	s.DB("unimaps").C("completeVenues").Find(bson.M{"_id": id}).One(&complete)
	return
}

func cacheDetails(complete CompleteUmObject) {
	err := s.DB("unimaps").C("completeVenues").Insert(complete)
	if err != nil {
		fmt.Println(err)
	}
	return
}
