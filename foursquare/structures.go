package foursquare

import (
	"time"
)

type DetailsApiResponse struct {
	Meta     MetaType            "meta"
	Response DetailsResponseType "response"
}

type DetailsResponseType struct {
	Venue CompleteVenue "venue"
}

type CompleteVenue struct {
	Id            string         "id"
	Name          string         "name"
	Contact       ContactType    "contact"
	Location      LocationType   "location"
	Categories    []CategoryType "categories"
	Verified      bool           "verified"
	Stats         StatsType      "stats"
	Url           string         "url"
	Menu          MenuType       "menu"
	Rating        float64        "rating"
	RatingSignals int            "ratingSignals"
	Photos        PhotosType     "photos"
	Tips          TipsType       "tips"
	Tags          []string       "tags"
	CanonicalUrl  string         "canonicalUrl"
	Description   string         "description"
	Hours         HoursType      "hours"
}

type HoursType struct {
	Status     string          "status"
	IsOpen     bool            "isOpen"
	Timeframes []TimeframeType "timeframes"
}

type TimeframeType struct {
	Days string     "days"
	Open []OpenType "open"
}

type OpenType struct {
	RenderedTime string "renderedTime"
}

type CompleteUmObject struct {
	Id           string       "_id"
	Loc          [2]float64   "loc"
	Name         string       "name"
	Contact      ContactType  "contact"
	Location     LocationType "location"
	Categories   []string     "categories"
	Stats        StatsType    "stats"
	Website      string       "url"
	Menu         MenuType     "menu"
	Rating       float64      "rating"
	Votes        int          "votes"
	Tips         []Tip        "tips"
	Updated      int64        "updated"
	Tags         []string     "tags"
	CanonicalUrl string       "canonicalUrl"
	Description  string       "description"
	Hours        HoursType    "hours"
}

func (v CompleteVenue) toUmObject() (cUm CompleteUmObject) {
	cUm.Id = v.Id
	cUm.Loc[0] = v.Location.Lat
	cUm.Loc[1] = v.Location.Lng
	cUm.Name = v.Name
	cUm.Contact = v.Contact
	cUm.Location = v.Location
	for _, c := range v.Categories {
		cUm.Categories = append(cUm.Categories, c.Id)
	}
	cUm.Stats = v.Stats
	cUm.Website = v.Url
	cUm.Menu = v.Menu
	cUm.Rating = v.Rating
	cUm.Votes = v.RatingSignals
	for _, group := range v.Tips.Groups {
		cUm.Tips = append(cUm.Tips, group.Items...)
	}
	cUm.Updated = time.Now().Unix()
	cUm.Tags = v.Tags
	cUm.CanonicalUrl = v.CanonicalUrl
	cUm.Description = v.Description
	cUm.Hours = v.Hours

	return
}

type Tip struct {
	Id        string    "id"
	CreatedAt float64   "created_at"
	Text      string    "text"
	Likes     LikesType "likes"
	User      UserType  "user"
}

type UserType struct {
	Id        string       "id"
	FirstName string       "firstName,omitempty"
	LastName  string       "lastName,omitempty"
	Photo     UserPhoto    "photo,omitempty"
	Gender    string       "gender,omitempty"
	HomeCity  string       "homeCity,omitempty"
	Bio       string       "bio,omitempty"
	Contact   UserContacts "contact"
}

type UserContacts struct {
	Twitter  string "twitter"
	Facebook string "facebook"
}

type UserPhoto struct {
	Prefix string "prefix,omitempty"
	Suffix string "suffix,omitempty"
	Allfix string "allfix,omitempty"
}

type LikesType struct {
	Count int "count"
}

type TipsType struct {
	Count  int         "count"
	Groups []TipsGroup "groups"
}

type TipsGroup struct {
	Type  string "type"
	Name  string "name"
	Count int    "count"
	Items []Tip  "items"
}

type Photo struct {
	Id        string      "id"
	Loc       [2]float64  "loc"
	Parent    string      "parent,omitempty"
	CreatedAt float64     "created_at"
	Prefix    string      "prefix,omitempty"
	Suffix    string      "suffix,omitempty"
	Allfix    string      "allfix,omitempty"
	Width     int         "width,omitempty"
	Height    int         "height,omitempty"
	UmSource  string      "umsource,omitempty"
	Link      string      "link,omitempty"
	User      PhotoAuthor "user"
}

type PhotoAuthor struct {
	Id        string    "id"
	Username  string    "username"
	FirstName string    "firstName"
	Photo     UserPhoto "photo"
}

type PhotosType struct {
	Count  int           "count"
	Groups []PhotosGroup "groups"
}

type PhotosGroup struct {
	Type  string  "type"
	Name  string  "name"
	Count int     "count"
	Items []Photo "items"
}

type VenuesApiResponse struct {
	Meta     MetaType           "meta"
	Response VenuesResponseType "response"
}

type VenuesResponseType struct {
	Venues []CompactVenue "venues"
}

type CompactUmObject struct {
	Id         string         "_id"
	Loc        [2]float64     "loc"
	Updated    float64        "updated"
	Name       string         "name"
	Contact    ContactType    "contact,omitempty"
	Location   LocationType   "location,omitempty"
	Categories []CategoryType "categories,omitempty"
	Verified   bool           "verified,omitempty"
	Stats      StatsType      "stats,omitempty"
	Menu       MenuType       "menu,omitempty"
	Website    string         "url,omitempty"
}

type TinyVenue struct {
	Id         string     "_id"
	Loc        [2]float64 "loc"
	Title      string     "title"
	Categories []string   "categories"
}

func (v CompactUmObject) toTiny() (cUm TinyVenue) {
	cUm.Id = v.Id
	cUm.Loc = v.Loc
	cUm.Title = v.Name
	for _, c := range v.Categories {
		cUm.Categories = append(cUm.Categories, c.Id)
	}
	return
}

type CompactVenue struct {
	Id         string         "id"
	Name       string         "name"
	Contact    ContactType    "contact"
	Location   LocationType   "location"
	Categories []CategoryType "categories"
	Verified   bool           "verified"
	Stats      StatsType      "stats"
	Menu       MenuType       "menu"
	Website    string         "url"
}

func (v CompactVenue) toUmObject() (cUm CompactUmObject) {
	cUm.Id = v.Id
	cUm.Loc[0] = v.Location.Lat
	cUm.Loc[1] = v.Location.Lng
	cUm.Name = v.Name
	cUm.Contact = v.Contact
	cUm.Location = v.Location
	cUm.Categories = v.Categories
	cUm.Verified = v.Verified
	cUm.Stats = v.Stats
	cUm.Menu = v.Menu
	cUm.Website = v.Website
	return
}

type MenuType struct {
	Type      string "type,omitempty"
	Url       string "url,omitempty"
	MobileUrl string "mobileUrl,omitempty"
}

type StatsType struct {
	CheckinsCount int "checkinsCount,omitempty"
	UsersCount    int "usersCount,omitempty"
	TipCount      int "tipCount,omitempty"
}

type CategoryType struct {
	Id         string   "id"
	Name       string   "name"
	PluralName string   "pluralName"
	ShortName  string   "shortName"
	Icon       IconType "icon"
	Primary    bool     "primary"
}

type IconType struct {
	Prefix string "prefix"
	Suffix string "suffix"
}

type LocationType struct {
	Address     string  "address,omitempty"
	CrossStreet string  "crossStreet,omitempty"
	Lat         float64 "lat"
	Lng         float64 "lng"
	PostalCode  string  "postalCode,omitempty"
	City        string  "city,omitempty"
	State       string  "state,omitempty"
	Country     string  "country,omitempty"
	CC          string  "cc,omitempty"
}

type MetaType struct {
	Code int "code"
}

type ContactType struct {
	Phone          string "phone,omitempty"
	FormattedPhone string "formattedPhone,omitempty"
	Twitter        string "twitter,omitempty"
}
