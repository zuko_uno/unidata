package foursquare

import (
	"encoding/json"
	"fmt"
	"launchpad.net/mgo"
	"net/http"
	"strconv"
	"strings"
)

var (
	s          *mgo.Session
	httpClient *http.Client
	response   string
)

func InitAndListen(session *mgo.Session, c *http.Client) {
	//инициализирует провайдер и вешает обработчики для http-запросов
	s = session
	httpClient = c
	http.HandleFunc("/fetch", fetch)
	http.HandleFunc("/venues", venues)
}

func fetch(w http.ResponseWriter, r *http.Request) {
	var (
		response string
		complete CompleteUmObject
	)

	//req params
	id := r.FormValue("id")

	cVenue := fetchDetails(id)
	complete = cVenue.toUmObject()
	if complete.Id == "" {
		response = "{}"
		return
	}
	response = genDetailedObject(complete)
	fmt.Fprint(w, response)

	go cacheDetails(complete)
	go cachePhotos(cVenue)
}

func genDetailedObject(obj CompleteUmObject) (response string) {
	bytes, err := json.Marshal(obj)
	if err != nil {
		return
	}
	fmt.Println("Generate response")
	response = string(bytes)
	return
}

func venues(w http.ResponseWriter, r *http.Request) {
	var (
		err      error
		response string
		box      [2][2]float64
	)

	//req params
	box[0][0], err = strconv.ParseFloat(r.FormValue("xlat"), 32)
	box[0][1], err = strconv.ParseFloat(r.FormValue("xlon"), 32)
	box[1][0], err = strconv.ParseFloat(r.FormValue("ylat"), 32)
	box[1][1], err = strconv.ParseFloat(r.FormValue("ylon"), 32)
	categories_string := r.FormValue("categories")
	categories := strings.Split(categories_string, ",")

	territory := r.FormValue("territory")

	if err != nil {
		fmt.Println(err)
	}

	//trying to find in cache
	venuesData := extractCachedVenues(box, categories, territory)
	response = genVenuesResponse(venuesData)
	fmt.Fprint(w, response)
}

func genVenuesResponse(venuesData []CompactUmObject) (response string) {
	var venues []TinyVenue

	for _, v := range venuesData {
		venues = append(venues, v.toTiny())
	}

	fmt.Println("Venues: generate vesponse")
	bytes, err := json.Marshal(venues)
	if err != nil {
		fmt.Println("Error, on marshalizing response struct")
		return
	}
	response = string(bytes)
	return
}

/*func venuesBackup(w http.ResponseWriter, r *http.Request) {
	var (
		lat, lon, rad float64
		err           error
		response      string
	)

	fmt.Println("Venues: START")

	//req params
	lat, err = strconv.ParseFloat(r.FormValue("lat"), 32)
	lon, err = strconv.ParseFloat(r.FormValue("lon"), 32)
	rad, err = strconv.ParseFloat(r.FormValue("rad"), 32)
	rad = rad * 0.000009

	if err != nil {
		return
	}

	//trying to find in cache
	venuesData := extractCachedVenues(lat, lon, rad)
	if len(venuesData) > 0 {
		fmt.Println("Venues: venues found in cache: ", len(venuesData))
		response = genVenuesResponse(venuesData)
	} else {
		//else - fetch from foursqare, cache, and genereate response
		fmt.Println("Venues: not found in cache - start fetching")
		venuesData = fetchVenues(lat, lon)
		if len(venuesData) > 0 {
			fmt.Println("Venues: recivied venues - ", len(venuesData))
		} else {
			fmt.Println("Venues: no venues found")
		}
	}

	response = genVenuesResponse(venuesData)

	fmt.Println("Venues: sending response to user")
	fmt.Fprint(w, response)
}
*/

/*func details(w http.ResponseWriter, r *http.Request) {
	var (
		response string
		complete CompleteUmObject
	)

	//req params
	id := r.FormValue("id")

	complete = extractCachedDetails(id)
	if complete.Id == "" {
		fmt.Println("Details: not found, start cache")
		cVenue := fetchDetails(id)
		complete = cVenue.toUmObject()
		if complete.Id == "" {
			response = "{}"
			return
		}
		//instagramm
		complete.Instaphotos = instagram.GetPhotos(complete.Location.Lat, complete.Location.Lng, 500.0)
		//wikipedia
		complete.Article = wikipedia.FetchArticle(complete.Location.Lat, complete.Location.Lng)
		cacheDetails(complete)
	}

	response = genDetailsResponse(complete)
	fmt.Fprint(w, response)
}
*/
/*func genDetailsResponse(obj CompleteUmObject) (response string) {
	bytes, err := json.Marshal(obj)
	if err != nil {
		return
	}
	fmt.Println("Generate response")
	response = string(bytes)
	return
}
*/
