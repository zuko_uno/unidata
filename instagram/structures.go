package instagram

import (
	"launchpad.net/mgo/bson"
)

type UmPhoto struct {
	Id        bson.ObjectId "_id"
	Loc       [2]float64    "loc"
	Thumbnail ImageType     "thumbnail"
	Low       ImageType     "low"
	Full      ImageType     "full"
	Text      string        "text"
	Author    AuthorType    "author"
	Location  LocationType  "location"
	Updated   int64         "updated"
}

func (i Instaphoto) toUmPhoto() (u UmPhoto) {
	u.Id = bson.NewObjectId()
	u.Loc[0] = i.Location.Latitude
	u.Loc[1] = i.Location.Longitude
	u.Thumbnail = i.Images.Thumbnail
	u.Low = i.Images.Low
	u.Full = i.Images.Standard
	if i.Caption != nil {
		u.Text = i.Caption.Text
		u.Author = i.Caption.From
	}
	u.Location = i.Location
	return
}

type PhotosApiResponse struct {
	Meta MetaType     "meta"
	Data []Instaphoto "data"
}

type MetaType struct {
	Code    int    `json:"code"`
	Error   string `json:"error_type"`
	Message string `json:"error_message"`
}

type Instaphoto struct {
	Location  LocationType "location"
	Timestamp string       `json:"created_time"`
	Link      string
	Images    ImagesType
	Caption   *CaptionType
}

type CaptionType struct {
	Timestamp string `json:"created_time"`
	Text      string
	From      AuthorType
}

type AuthorType struct {
	Username string
	Avatar   string `json:"profile_picture"`
	Id       string
	FullName string `json:"full_name"`
}

type ImagesType struct {
	Low       ImageType `json:"low_resolution"`
	Thumbnail ImageType `json:"thumbnail"`
	Standard  ImageType `json:"standard_resolution"`
}

type ImageType struct {
	Url           string
	Width, Height int
}

type LocationType struct {
	Latitude  float64
	Longitude float64
	Name      string
	Id        int
}
