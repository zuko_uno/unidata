package instagram

import (
	"fmt"
	"launchpad.net/mgo/bson"
)

func extractCachedPhotos(lat, lon, rad float64) (photos []UmPhoto) {
	var photo UmPhoto
	fmt.Println("Photos: extracting from cache...")
	ll := []float64{lat, lon}
	query := s.DB("unimaps").C("instaPhotos").
		Find(bson.M{"loc": bson.M{"$near": ll, "$maxDistance": rad}})
	count, err := query.Count()
	if err != nil {
		fmt.Println("Error, while use count on query")
	}
	fmt.Println("Photos: found ", count)
	if count > 0 {
		iPhotos := query.Iter()
		for {
			if !iPhotos.Next(&photo) {
				break
			}
			photos = append(photos, photo)
		}
	}
	return
}

func cachePhotos(photosData []UmPhoto) {
	fmt.Println("Cache photos: saving photos")
	for _, photo := range photosData {
		err := s.DB("unimaps").C("instaPhotos").Insert(photo)
		if err != nil {
			fmt.Println(err)
		}
	}
	return
}
