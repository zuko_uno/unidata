package instagram

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strconv"
	"time"
)

const (
	photosEndpoint = "https://api.instagram.com/v1/media/search?"
	distance       = "&distance=5000"
	creds          = "&client_id=b727c07d292e4dc7811e769128045318&client_secret=c081f91ec46c410385fcb6820ace393c"
)

func fetchPhotos(lat, lon float64) (photosData []UmPhoto) {
	var photos PhotosApiResponse

	apiUrl := photosEndpoint + prepareLL(lat, lon) + distance + creds

	fmt.Println("Fetch photos: start")
	//fetch and parse
	response, err := httpClient.Get(apiUrl)
	if err != nil {
		fmt.Println("Fetch venues: http get error, reconnect")
		i := 0
		for {
			var err_reconnect error
			i++
			time.Sleep(10 * time.Millisecond)
			response, err_reconnect = httpClient.Get(apiUrl)
			if err_reconnect == nil {
				break
			}
			if err_reconnect != nil && i >= 2 {
				return
			}
		}
	}

	jsonData, err := ioutil.ReadAll(response.Body)
	response.Body.Close()

	if err != nil {
		fmt.Println("Fetch photos: error while reading response")
		return
	}

	err = json.Unmarshal(jsonData, &photos)

	if err != nil {
		fmt.Println("Fetch venues: parsing json: ", err, string(jsonData))
		return
	}

	photosData = apiPhotos2um(photos.Data)


	return
}

func apiPhotos2um(apiData []Instaphoto) (photosData []UmPhoto) {
	for _, compact := range apiData {
		photosData = append(photosData, compact.toUmPhoto())
	}
	return
}

func prepareLL(lat, lon float64) (ll string) {
	fmt.Println("Preparing LL")
	ll += "&lat=" + strconv.FormatFloat(lat, 'f', -1, 64)
	ll += "&lng=" + strconv.FormatFloat(lon, 'f', -1, 64)
	return
}
