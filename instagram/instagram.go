package instagram

import (
	"encoding/json"
	"fmt"
	"launchpad.net/mgo"
	"net/http"
	"strconv"
)

var (
	s          *mgo.Session
	httpClient *http.Client
	response   string
)

func InitAndListen(session *mgo.Session, c *http.Client) {
	s = session
	httpClient = c
	http.HandleFunc("/photos", photos)
}

func GetPhotos(lat, lon, rad float64) (photosData []UmPhoto) {
	//trying to find in cache
	photosData = extractCachedPhotos(lat, lon, rad)
	if len(photosData) > 0 {
		fmt.Println("photos: photos found in cache: ", len(photosData))
		response = genPhotosResponse(photosData)
	} else {
		//else - fetch from foursqare, cache, and genereate response
		fmt.Println("photos: not found in cache - start fetching")
		photosData = fetchPhotos(lat, lon)
		fmt.Println("Fetch photos: call cache in routine")
		go cachePhotos(photosData)
		if len(photosData) > 0 {
			fmt.Println("photos: recivied photos - ", len(photosData))
		} else {
			fmt.Println("photos: no venues found")
		}
	}
	return
}

func photos(w http.ResponseWriter, r *http.Request) {
	var (
		lat, lon, rad float64
		err           error
		response      string
	)

	fmt.Println("Photos: START")

	//req params
	lat, err = strconv.ParseFloat(r.FormValue("lat"), 32)
	lon, err = strconv.ParseFloat(r.FormValue("lon"), 32)
	rad, err = strconv.ParseFloat(r.FormValue("rad"), 32)
	rad = rad * 0.000009

	if err != nil {
		return
	}

	photosData := GetPhotos(lat, lon, rad)
	response = genPhotosResponse(photosData)

	fmt.Println("Venues: sending response to user")
	fmt.Fprint(w, response)
}

func genPhotosResponse(photosData []UmPhoto) (response string) {
	fmt.Println("Venues: generate vesponse")
	bytes, err := json.Marshal(photosData)
	if err != nil {
		fmt.Println("Error, on marshalizing response struct")
		return
	}
	response = string(bytes)
	return
}
